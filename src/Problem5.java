import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String str;
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please input: ");
            str = sc.nextLine();
            System.out.println(str);
            if(str.trim().equalsIgnoreCase("bye")){
                System.out.print("Exit program");
                System.exit(0);
            }
        }
    }
}
